from os.path import dirname, abspath
import os

# 1: abspath(__file__) obtain the absolute path of the script
# 2: walk up the directory tree by calling dirname
API_DIR = dirname(dirname(abspath(__file__)))
RES_DIR = API_DIR + "/resources/"

TRAIN_DATAPATH = RES_DIR + "/data/X_train.csv"
Y_TRAIN_DATAPATH = RES_DIR + "/data/y_train.csv"

TESTING_DATA_FILE = RES_DIR + "/data/X_test.csv"
Y_TESTING_DATA_FILE = RES_DIR + "/data/y_test.csv"

MODELS_PATH = RES_DIR + "/models/"
METRIC_PATH = MODELS_PATH


# features config

FEATURES_ALL = ['Age', 'Sex', 'SibSp', 'Parch','Embarked', 'Pclass', 'Fare']
FEATURES_TARGET = ['Survived']

FEATURES_NUMERICAL = ['Age', 'SibSp', 'Parch', 'Fare']
FEATURES_CATEGORICAL = ['Sex', 'Embarked', 'Pclass']

# LOGGER CONFIG
LOGGER_NAME = 'API'

# Define config class
class Config:
    TESTING = False
    DEBUG = False
    # CSRF stands for Cross Site Request Forgery (security)
    CSRF_ENABLED = True
    SECRET_KEY = 'change-this-key'
    SERVER_PORT = 5000

# Define DevelopmentConfig
class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True

# errors
OK = 0
FILE_NOT_EXIST = 2
UNKNOWN_MODEL = 3
NOCREDENTIALS = 4
