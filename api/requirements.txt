python-dotenv==0.14.0
scikit-learn==0.23.2
pandas==1.1.2
uwsgi==2.0.19.1
pytest==5.4.3
joblib==0.17.0
boto3==1.16.9
Flask==1.1.2