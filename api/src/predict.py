import logging
import joblib
import pandas as pd
import numpy as np

from api.configs import app_config as app_conf 

logger_api = logging.getLogger(app_conf.LOGGER_NAME)

def prediction(*, input_data:pd.DataFrame, model_name:str='knn') -> dict:
    """
    Parameters:
        input_data          (pd.DataFrame):     Array of model prediction inputs.
        model_name          (str):              model name selected

    Returns:
                            (dict):             Predictions for each input row.
    """
    
    try:
        pipeline_model = joblib.load(filename=app_conf.MODELS_PATH+'result_{0}.pkl'.format(model_name))
    except Exception as e:
        logger_api.exception (f'Model:%s - Exception info: %s ',model_name,e)
        raise e
    
    # fill_value = ?
    input_data = input_data.reindex(columns=app_conf.FEATURES_ALL)
    predictions = pipeline_model.predict(input_data[app_conf.FEATURES_ALL])
    
    results =  {
        'prediction': tuple(map(int, predictions)),
        'model_name': model_name,
    }
   
    return results