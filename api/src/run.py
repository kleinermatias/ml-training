from api.src import app as app
from api.configs import app_config as appconf

application = app.def_app(
    config_object=appconf.DevelopmentConfig)

if __name__ == '__main__':
    application.run()