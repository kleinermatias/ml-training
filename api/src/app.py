import logging
import joblib
import logging
import pandas as pd
import random
from api.src import predict as pr
from api.configs import app_config as app_conf
from flask import Blueprint, jsonify, request, make_response, Flask

logger_api = logging.getLogger(app_conf.LOGGER_NAME)


prediction_app = Blueprint('prediction_app', __name__)

# A/B Testing. Create a list with a specified amount of elements.
my_models_list = ['knn'] * 70 + ['dtree'] * 30


@prediction_app.route('/models', methods=['GET'])
def health():
    response = make_response(jsonify(["knn", "dtree"]),200)
    response.headers["Content-Type"] = "application/json"
    return response


@prediction_app.route('/prediction', methods=['POST'])
def predict():
    
    logger_api.info (f'call prediction')
    
    try:
        json_data = request.get_json()
    except Exception as e:
        response = make_response(jsonify({"ERROR:": str(e)}),400)
        response.headers["Content-Type"] = "application/json"
        logger_api.error(f'error on prediction')
        return response
    
    try:
        X = pd.DataFrame(json_data)
    except Exception as e:
        response = make_response(jsonify({"ERROR:": str(e)}),400)
        response.headers["Content-Type"] = "application/json"
        logger_api.error(f'error on prediction')
        return response

    try:
        prediction = pr.prediction(input_data=X,model_name=random.choice(my_models_list))
    except Exception as e:
        response = make_response(jsonify({"ERROR:": str(e)}),400)
        response.headers["Content-Type"] = "application/json"
        logger_api.error(f'error on prediction')
        return response

    response = make_response(jsonify(prediction),200)
    response.headers["Content-Type"] = "application/json"
    logger_api.info(f'success predict')

    return response


def def_app(*, config_object) -> Flask:
    """define a flask app instance."""
    flask_app = Flask('api_testml')
    flask_app.config.from_object(config_object)

    # register blueprint
    flask_app.register_blueprint(prediction_app)

    return flask_app