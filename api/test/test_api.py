import os
import tempfile
import pytest
from api.src import app as app
from api.configs import app_config as appconf
from flask import Flask
import json
import pandas as pd


@pytest.fixture
def client():
    application = app.def_app(
    config_object=appconf.DevelopmentConfig)
    with application.test_client() as client:
        with application.app_context():
            yield application

@pytest.fixture
def flask_test_client(client):
    with client.test_client() as test_client:
        yield test_client


def test_app_endpoint_unknown(flask_test_client):
    response = flask_test_client.get('/unknown')
    assert response.status_code == 404

def test_app_endpoint_models(flask_test_client):
    response = flask_test_client.get('/models')
    assert response.status_code == 200


def test_app_endpoint_predictions(flask_test_client):
    
    test_data = pd.read_csv(appconf.TESTING_DATA_FILE)
    #encode df using records. -> index labels are not preserved with this encoding.
    post_json = test_data.to_json(orient='records')

    # get /predictions response  
    response = flask_test_client.post('/prediction',
                                      json=json.loads(post_json))
    assert response.status_code == 200

    # get /predictions data and apply json loads
    response_json = json.loads(response.data)
    # get only 'prediction' in response.data
    prediction = response_json['prediction']
    # check predictions based on test data
    assert prediction[0] == 0
    assert prediction[1] == 1