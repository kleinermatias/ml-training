import logging
import os
from dotenv import load_dotenv
from api.configs import app_config as app_conf

logger_classifier = logging.getLogger(__name__)

def upload_s3(*,file_path:str,
                file_name:str,
                S3_ACCESS_KEY:str,
                S3_SECRET_KEY:str,
                BUCKET:str
                )-> None: 
    """     upload file to s3 bucket
    Parameters:
        file_path       (str):  path to file
        file_name       (str):  name in bucket
        S3_ACCESS_KEY   (str):  S3_ACCESS_KEY
        S3_SECRET_KEY   (str):  S3_SECRET_KEY
        BUCKET          (str):  BUCKET name

    Returns:
                            (int):  status value
    """
    
    import boto3
    from botocore.exceptions import NoCredentialsError
    s3 = boto3.client(  's3',
                        aws_access_key_id=S3_ACCESS_KEY,
                        aws_secret_access_key=S3_SECRET_KEY
                     )

    try:
        s3.upload_file(file_path, BUCKET, file_name)
        return app_conf.OK
    except FileNotFoundError:
        return app_conf.FILE_NOT_EXIST
    except NoCredentialsError:
        return app_conf.NOCREDENTIALS

if __name__ == '__main__':
    
    load_dotenv()
    
    S3_ACCESS_KEY = os.getenv('S3_ACCESS_KEY')
    S3_SECRET_KEY = os.getenv('S3_SECRET_KEY')
    BUCKET = os.getenv('BUCKET')
    
    if (S3_ACCESS_KEY):
        
        upload_s3(  file_path = app_conf.MODELS_PATH+'result_{0}.pkl'.format("knn"),
                    file_name = 'models/'+'result_{0}.pkl'.format("knn"), 
                    S3_ACCESS_KEY = S3_ACCESS_KEY,
                    S3_SECRET_KEY = S3_SECRET_KEY,
                    BUCKET = BUCKET
                    )
        upload_s3(  file_path = app_conf.MODELS_PATH+'result_{0}.pkl'.format("dtree"),
                    file_name = 'models/'+'result_{0}.pkl'.format("dtree"), 
                    S3_ACCESS_KEY = S3_ACCESS_KEY,
                    S3_SECRET_KEY = S3_SECRET_KEY,
                    BUCKET = BUCKET
                    )

        upload_s3(  file_path = app_conf.TRAIN_DATAPATH,
                    file_name = 'datasets/'+'X_train.csv',
                    S3_ACCESS_KEY = S3_ACCESS_KEY,
                    S3_SECRET_KEY = S3_SECRET_KEY,
                    BUCKET = BUCKET
                    )
        upload_s3(  file_path = app_conf.Y_TRAIN_DATAPATH,
                    file_name = 'datasets/'+'y_train.csv',
                    S3_ACCESS_KEY = S3_ACCESS_KEY,
                    S3_SECRET_KEY = S3_SECRET_KEY,
                    BUCKET = BUCKET
                    )
        upload_s3( file_path = app_conf.TESTING_DATA_FILE,
                    file_name = 'datasets/'+'X_test.csv',
                    S3_ACCESS_KEY = S3_ACCESS_KEY,
                    S3_SECRET_KEY = S3_SECRET_KEY,
                    BUCKET = BUCKET
                    )
        upload_s3(  file_path = app_conf.Y_TESTING_DATA_FILE,
                    file_name = 'datasets/'+'y_test.csv',
                    S3_ACCESS_KEY = S3_ACCESS_KEY,
                    S3_SECRET_KEY = S3_SECRET_KEY,
                    BUCKET = BUCKET
                    )