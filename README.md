# ML TRAINING RAPPI

[![CircleCI](https://circleci.com/bb/kleinermatias/ml-training.svg?style=shield&circle-token=5d416805c43ea201453d985ccd7e9cea1dbfe76d)](https://app.circleci.com/pipelines/bitbucket/kleinermatias/ml-training)

Training challenge based on Titanic Dataset 

# Docker 

## build PIPELINE

```
$ cd classifier
$ docker build -t testml-pipeline .
$ docker run testml-pipeline
```

## build API

```
$ cd api
$ docker build -t testml-api .
$ docker run -p <port>:5000 testml-api
```


### Dockercompose

```
$ docker-compose up
```

* [GRAYLOG]  	 - http://127.0.0.1:9000
* [cAdvisor]      	 - http://127.0.0.1:8080
* [API - TITANIC]    - <5001>


# RUN LOCAL

## install dependencies
```
pip install -r classifier/requirements.txt
pip install -r api/requirements.txt
```
## Training PIPELINE

```
$ cd classifier
$ export PYTHONPATH=$PYTHONPATH:$PWD/classifier
$ python src/classifier.py <models>  #models: knn, dtree
```

## Run API

```
$ export PYTHONPATH=$PYTHONPATH:$PWD/:$PWD/api:$PWD/api/src
$ gunicorn -b :<port> run:application
```



# ENDPOINTS

## /models (GET)
used for testing, displays a list of available models.

## /prediction (POST)
given a list of passengers, predict if they survive. (1 yes, 0 no)

### example

#### post:
```
$ curl localhost:5000/prediction -d '[
        {"Pclass": 1, "Name": "Reuchlin, Jonkheer. John George", "Sex": "male", "Age":38.0, "SibSp":0,"Parch":0,"Ticket":19972,"Fare":0.0,"Cabin":"","Embarked":"S"},
        {"Pclass": 3, "Name": "Boulos, Miss. Nourelain", "Sex": "female", "Age":9.0, "SibSp":1,"Parch":1,"Ticket":2678,"Fare":15.2458,"Cabin":"","Embarked":"C"},
        {"Pclass": 1, "Name": "Andrews, Mr. Thomas Jr", "Sex": "male", "Age":39.0, "SibSp":0,"Parch":0,"Ticket":112050,"Fare":0.0,"Cabin":"A36","Embarked":"S"},
        {"Pclass": 2, "Name": "Christy, Miss. Julie Rachel", "Sex": "female", "Age":25.0, "SibSp":1,"Parch":1,"Ticket":237789,"Fare":30.0,"Cabin":"","Embarked":"C"},
        {"Pclass": 1, "Name": "Butt, Major. Archibald Willingham", "Sex": "male", "Age":45.0, "SibSp":0,"Parch":0,"Ticket":113050,"Fare":26.55,"Cabin":"B38","Embarked":"S"}
]' -H 'Content-Type: application/json'
```

#### response:
```
{
  "model_name": "knn", 
  "prediction": [
    0, 
    1, 
    0, 
    1, 
    0
  ]
}

```