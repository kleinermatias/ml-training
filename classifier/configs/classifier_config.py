from os.path import dirname, abspath

# path config

# 1: abspath(__file__) obtain the absolute path of the script
# 2: walk up the directory tree by calling dirname
CLASSIFIER_DIR = dirname(dirname(abspath(__file__)))
RES_DIR = CLASSIFIER_DIR + "/resources/"

TRAIN_DATAPATH = RES_DIR + "/data/X_train.csv"
Y_TRAIN_DATAPATH = RES_DIR + "/data/y_train.csv"

TESTING_DATA_FILE = RES_DIR + "/data/X_test.csv"
Y_TESTING_DATA_FILE = RES_DIR + "/data/y_test.csv"

OUTPUT_DATAPATH = RES_DIR + "/models/"
METRIC_PATH = OUTPUT_DATAPATH


# features config

FEATURES_ALL = ['Age', 'Sex', 'SibSp', 'Parch','Embarked', 'Pclass', 'Fare']
FEATURES_TARGET = ['Survived']

FEATURES_NUMERICAL = ['Age', 'SibSp', 'Parch', 'Fare']
FEATURES_CATEGORICAL = ['Sex', 'Embarked', 'Pclass']


# errors
OK = 0

# MODELS CONFIG
KNN_NEIGHBORS_MODEL = 3

# acceptable ratio in test
DTREE_MODEL_RATIO = 0.70
KNN_MODEL_RATIO = 0.70


# LOGGER CONFIG
LOGGER_NAME = 'PIPELINE'