from sklearn.pipeline import Pipeline
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.compose import ColumnTransformer
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import OneHotEncoder
import configs.classifier_config as cl_conf

numerical_transformer = Pipeline(
    steps = [
        ('imputer', SimpleImputer(strategy='mean')),
        #('scaler', StandardScaler()),
    ]
)

categorical_transformer = Pipeline(
    steps = [
        ('imputer', SimpleImputer(strategy='most_frequent')),
        ('onehot', OneHotEncoder(handle_unknown='ignore')),
    ]
)

prepare = ColumnTransformer(
    transformers = [
        ('numerical_transformer', numerical_transformer, cl_conf.FEATURES_NUMERICAL),
        ('categorical_transformer', categorical_transformer, cl_conf.FEATURES_CATEGORICAL)
    ]
)



# pipeline with 2 stages
#    first:     prepare dataset
#    second:    classifier

def training_model(*, model='knn') -> Pipeline:
    
    if (model=='knn'):
        pipeline_training = Pipeline(
            steps = [
                ('prepare', prepare),
                ('classifier', KNeighborsClassifier(n_neighbors = cl_conf.KNN_NEIGHBORS_MODEL))
            ]
        )
        return pipeline_training
    
    elif (model=='dtree'):
        pipeline_training = Pipeline(
            steps = [
                ('prepare', prepare),
                ('classifier', DecisionTreeClassifier())
            ]
        )
        return pipeline_training
    
    else: 
        raise Exception("UNKNOWN_MODEL")

    