import logging
from configs import classifier_config as cl_conf
import sys

logger_classifier = logging.getLogger(cl_conf.LOGGER_NAME)

def classifier  (   *, 
                    X_train_datapath:str = cl_conf.TRAIN_DATAPATH,
                    y_train_datapath:str = cl_conf.Y_TRAIN_DATAPATH, 
                    model_name:str = 'knn',
                    model_path:str = cl_conf.OUTPUT_DATAPATH
                ) -> int:

    """     generate the pkl object for a given model
    Parameters:
        X_train_datapath    (str):  path to training dataset - X
        y_train_datapath    (str):  path to training dataset - Y
        model_name          (str):  model name selected
        model_path          (str):  directory where pkl will be saved

    Returns:
                            (int):  status value
    """
    import joblib
    import pandas as pd
    from src import training_pipeline as train_pp
    import json
    
    logger_classifier.info(f'call classifier pipeline')
    
    #read dataset
    try:
        X_data_train = pd.read_csv(X_train_datapath)
        y_data_train = pd.read_csv(y_train_datapath)
    except Exception as e:
        logger_classifier.exception (f'Model:%s - Exception info: %s ',model_name,e)
        raise e
    
    #model training
    try:
        pipeline = train_pp.training_model(model=model_name)
    except Exception as e:
        logger_classifier.exception (f'Model:%s - Exception info: %s ',model_name,e)
        raise e
    
    # .values will give the values in an array. (shape: (n,1)
    # .ravel will convert that array shape to (n, )
    pipeline.fit(X_data_train[cl_conf.FEATURES_ALL], y_data_train.values.ravel())
    
    # save training model as object
    joblib.dump(value=pipeline, filename=model_path+'result_{0}.pkl'.format(model_name))
    logger_classifier.info(f'pkl model saved, name: %s ','result_{0}.pkl'.format(model_name))
    
    #save score as json
    with open(cl_conf.METRIC_PATH+'metrics_{0}.json'.format(model_name), 'w') as metrics_file:
        y_test = pd.read_csv(cl_conf.Y_TESTING_DATA_FILE)
        X_test = pd.read_csv(cl_conf.TESTING_DATA_FILE)
        score = pipeline.score(X_test[cl_conf.FEATURES_ALL], y_test.values.ravel())
        json.dump({'score': score}, metrics_file)
        logger_classifier.info(f'model name: %s, model score: %s',model_name,str(score))
    
    return cl_conf.OK


if __name__ == '__main__':
    if (('help' in sys.argv) or (len(sys.argv)==1)):
        print('Models:\n   {0}\n   {1} \nUsage: classifier.py <model>'.format('knn', 'dtree'))
    else:
        for i, arg in enumerate(sys.argv[1::]):
            status = classifier(model_name=arg)