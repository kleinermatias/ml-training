import logging
import joblib
import pandas as pd
import numpy as np

from configs import classifier_config as cl_conf 

logger_classifier = logging.getLogger(cl_conf.LOGGER_NAME)

def prediction(*, input_data:pd.DataFrame, model_name:str='knn') -> dict:
    """
    Parameters:
        input_data          (pd.DataFrame):     Array of model prediction inputs.
        model_name          (str):              model name selected

    Returns:
                            (dict):             Predictions for each input row.
    """
    

    try:
        pipeline_model = joblib.load(filename=cl_conf.OUTPUT_DATAPATH+'result_{0}.pkl'.format(model_name))
    except Exception as e:
        logger_classifier.exception (f'Model:%s - Exception info: %s ',model_name,e)
        return cl_conf.FILE_NOT_EXIST
    
    # fill_value = ?
    input_data = input_data.reindex(columns=cl_conf.FEATURES_ALL)
    predictions = pipeline_model.predict(input_data[cl_conf.FEATURES_ALL])
    
    results =  {
        'prediction': tuple(map(int, predictions)),
        'model_name': model_name,
    }

   
    return results