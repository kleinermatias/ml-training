import logging
import logging.handlers
from configs import classifier_config as cl_conf
import socket
from os import environ
import os

# Configure rsyslog addr
rsyslog_addr='localhost'
rsyslog_addr_port='514'
if environ.get('RSYSLOG') is not None:
    rsyslog_addr=os.getenv('RSYSLOG')
if environ.get('RSYSLOG_PORT') is not None:
    rsyslog_addr_port=os.getenv('RSYSLOG_PORT')

# Configure logger for use in package
logger = logging.getLogger(cl_conf.LOGGER_NAME)
sh = logging.handlers.SysLogHandler(
                                    address=(rsyslog_addr,int (rsyslog_addr_port)), 
                                    socktype=socket.SOCK_DGRAM
                                    )
logger.setLevel(level=logging.DEBUG)
sh.setLevel(level=logging.DEBUG)
sf = logging.Formatter('[%(levelname)s] - %(name)s - : %(message)s in %(pathname)s:%(lineno)d')
sh.setFormatter(sf)
logger.addHandler(sh)