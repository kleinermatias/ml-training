import os
import pytest
from src import predict as pr
from src import classifier as cl
from configs import classifier_config as cl_conf
from sklearn.metrics import accuracy_score

@pytest.fixture
def knn_model():
   return "knn"

@pytest.fixture
def dtree_model():
   return "dtree"

@pytest.fixture
def model_name_unknown():
   return "model_name_fail"

@pytest.fixture
def file_not_exist():
   return "datapath_fail"

def test_classifier_UnknowModel(model_name_unknown):
    # check invalid model name
   with pytest.raises(Exception) as e:
      assert  (cl.classifier(model_name=model_name_unknown))
   assert e.type == Exception


def test_classifier_FileNotExist(file_not_exist):
    # check invalid datapath
   with pytest.raises(Exception) as e:
      assert (cl.classifier(X_train_datapath=file_not_exist))
   assert e.type == FileNotFoundError

   with pytest.raises(Exception) as e:
      assert (cl.classifier(y_train_datapath=file_not_exist))
   assert e.type == FileNotFoundError


def test_classifier_knn(knn_model):
    # check knn pkl exist
    cl.classifier(model_name=knn_model)
    assert os.path.isfile(cl_conf.OUTPUT_DATAPATH+'result_{0}.pkl'.format(knn_model))

def test_classifier_dtree(dtree_model):
    # check dtree pkl exist 
    cl.classifier(model_name=dtree_model)
    assert os.path.isfile(cl_conf.OUTPUT_DATAPATH+'result_{0}.pkl'.format(dtree_model))


def test_predictions_dtree(dtree_model):
   import pandas as pd
   # load datasets
   y_test = pd.read_csv(cl_conf.Y_TESTING_DATA_FILE)
   X_test = pd.read_csv(cl_conf.TESTING_DATA_FILE)

   # get predictions
   subject = pr.prediction(input_data=X_test,model_name=dtree_model)
   
   # compare whit y_test
   assert accuracy_score(y_test,subject.get('prediction')) > cl_conf.DTREE_MODEL_RATIO
   assert len(subject.get('prediction')) == len(y_test.index)

def test_predictions_knn(knn_model):
   import pandas as pd
   # load datasets
   y_test = pd.read_csv(cl_conf.Y_TESTING_DATA_FILE)
   X_test = pd.read_csv(cl_conf.TESTING_DATA_FILE)

   # get predictions
   subject = pr.prediction(input_data=X_test,model_name=knn_model)
   
   # compare whit y_test
   assert accuracy_score(y_test,subject.get('prediction')) > cl_conf.KNN_MODEL_RATIO
   assert len(subject.get('prediction')) == len(y_test.index)