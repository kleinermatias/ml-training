python-dotenv==0.14.0
joblib==0.17.0
pandas==1.1.2
scikit-learn==0.23.2
pytest==5.4.3
boto3==1.16.9
scalene==1.1.4
python-dotenv==0.14.0