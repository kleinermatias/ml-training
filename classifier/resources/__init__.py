import logging
from configs import logger_config as log_conf
# Configure logger for use in package

logFormatter = '%(asctime)s - %(levelname)s %(funcName)s:%(lineno)d - %(message)s'
logging.basicConfig(format=logFormatter, level=logging.DEBUG)
logging.getLogger('boto3').setLevel(logging.DEBUG)
logging.getLogger('botocore').setLevel(logging.DEBUG)
logging.getLogger('s3transfer').setLevel(logging.DEBUG)
logging.getLogger('urllib3').setLevel(logging.DEBUG)