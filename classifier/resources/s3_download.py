import logging
import os
from dotenv import load_dotenv
from configs import classifier_config as cl_conf

logger_classifier = logging.getLogger(__name__)

def download_s3 ( *,
                file_path:str,
                file_name:str,
                S3_ACCESS_KEY:str,
                S3_SECRET_KEY:str,
                BUCKET:str
                )-> None: 
    """     download file from s3 bucket
    Parameters:
        file_path       (str):  path to file in bucket
        file_name       (str):  path to file in local
        S3_ACCESS_KEY   (str):  S3_ACCESS_KEY
        S3_SECRET_KEY   (str):  S3_SECRET_KEY
        BUCKET          (str):  BUCKET name

    Returns:
                            (int):  status value
    """
    
    import boto3
    from botocore.exceptions import NoCredentialsError
    s3 = boto3.client(  's3',
                        aws_access_key_id=S3_ACCESS_KEY,
                        aws_secret_access_key=S3_SECRET_KEY
                     )

    try:
        s3.download_file(BUCKET, file_path, file_name)
        return cl_conf.OK
    except FileNotFoundError:
        return cl_conf.FILE_NOT_EXIST
    except NoCredentialsError:
        return cl_conf.NOCREDENTIALS

if __name__ == '__main__':
    
    load_dotenv()
    
    S3_ACCESS_KEY = os.getenv('S3_ACCESS_KEY')
    S3_SECRET_KEY = os.getenv('S3_SECRET_KEY')
    BUCKET = os.getenv('BUCKET')
    
    if (S3_ACCESS_KEY):
        
        download_s3(  
                    file_name = cl_conf.OUTPUT_DATAPATH+'result_{0}.pkl'.format("knn"),
                    file_path = 'models/'+'result_{0}.pkl'.format("knn"), 
                    S3_ACCESS_KEY = S3_ACCESS_KEY,
                    S3_SECRET_KEY = S3_SECRET_KEY,
                    BUCKET = BUCKET
                    )
        download_s3(
                    file_name  = cl_conf.OUTPUT_DATAPATH+'result_{0}.pkl'.format("dtree"),
                    file_path = 'models/'+'result_{0}.pkl'.format("dtree"), 
                    S3_ACCESS_KEY = S3_ACCESS_KEY,
                    S3_SECRET_KEY = S3_SECRET_KEY,
                    BUCKET = BUCKET
                    )

        download_s3(
                    file_name = cl_conf.TRAIN_DATAPATH,
                    file_path = 'datasets/'+'X_train.csv',
                    S3_ACCESS_KEY = S3_ACCESS_KEY,
                    S3_SECRET_KEY = S3_SECRET_KEY,
                    BUCKET = BUCKET
                    )
        download_s3(
                    file_name= cl_conf.Y_TRAIN_DATAPATH,
                    file_path = 'datasets/'+'y_train.csv',
                    S3_ACCESS_KEY = S3_ACCESS_KEY,
                    S3_SECRET_KEY = S3_SECRET_KEY,
                    BUCKET = BUCKET
                    )
        download_s3(
                    file_name = cl_conf.TESTING_DATA_FILE,
                    file_path = 'datasets/'+'X_test.csv',
                    S3_ACCESS_KEY = S3_ACCESS_KEY,
                    S3_SECRET_KEY = S3_SECRET_KEY,
                    BUCKET = BUCKET
                    )
        download_s3( 
                    file_name = cl_conf.Y_TESTING_DATA_FILE,
                    file_path = 'datasets/'+'y_test.csv',
                    S3_ACCESS_KEY = S3_ACCESS_KEY,
                    S3_SECRET_KEY = S3_SECRET_KEY,
                    BUCKET = BUCKET
                    )